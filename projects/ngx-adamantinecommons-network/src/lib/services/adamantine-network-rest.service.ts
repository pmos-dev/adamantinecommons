import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';
import {
		ICommonsNetworkPacket,
		isICommonsNetworkPacket,
		ICommonsNetworkPacketCompressed,
		isICommonsNetworkPacketCompressed,
		compressPacket,
		decompressPacket
} from 'tscommons-network';

import { CommonsRestService } from 'ngx-httpcommons-rest';
import { CommonsNetworkService } from 'ngx-httpcommons-network';

interface IRestNetworkPacket extends ICommonsNetworkPacket {
	packet: number;
}
function isIRestNetworkPacket(test: any, dateAsString: boolean = false): test is IRestNetworkPacket {
	if (!isICommonsNetworkPacket(test, dateAsString)) return false;

	const attempt: IRestNetworkPacket = test as IRestNetworkPacket;
	
	if (!CommonsType.hasPropertyNumber(attempt, 'packet')) return false;
	
	return true;
}
	
interface IRestNetworkPacketCompressed extends ICommonsNetworkPacketCompressed {
	p: number;
}
function isIRestNetworkPacketCompressed(test: any): test is IRestNetworkPacketCompressed {
	if (!isICommonsNetworkPacketCompressed(test)) return false;

	const attempt: IRestNetworkPacketCompressed = test as IRestNetworkPacketCompressed;
	
	if (!CommonsType.hasPropertyNumber(attempt, 'p')) return false;
	
	return true;
}
	
class RestService extends CommonsRestService {
	constructor(
			rootURL: string,
			http: HttpClient
	) {
		super(rootURL, http);
	}
}

export class AdamantineNetworkRestService extends CommonsNetworkService {
	restService: RestService;
	
	active: boolean = false;
	since: number = -1;
	
	constructor(
			url: string,
			http: HttpClient,
			interval: number,
			private compress: boolean = false
	) {
		super();
		
		this.restService = new RestService(
				url,
				http
		);

		let completed: boolean = true;
		setInterval(async (): Promise<void> => {
			if (!completed) return;
			if (!this.active) return;
			
			if (this.since === -1) {
				this.since = await this.restService.get<number>(`packets?channel=${this.getChannel()}`);
			}
			
			try {
				const packets: TEncodedObject[] = await this.restService.get<TEncodedObject[]>(`packets/${this.since}/${this.getDeviceId()}?channel=${this.getChannel()}&z=${this.compress ? '1' : '0'}`);
				for (const incoming of packets) {
					const decoded: TPropertyObject = CommonsType.decodePropertyObject(incoming);

					let packet: ICommonsNetworkPacket;
					
					if (this.compress) {
						if (!isIRestNetworkPacketCompressed(decoded)) {
							console.log('Received an invalid compressed REST network packet');
							continue;
						}
						
						const compressed: IRestNetworkPacketCompressed = decoded as IRestNetworkPacketCompressed;
						this.since = Math.max(this.since, compressed.p);
						
						if (compressed.z !== undefined) compressed.z = JSON.parse(compressed.z);
	
						packet = decompressPacket(compressed);
					} else {
						if (!isIRestNetworkPacket(decoded)) {
							console.log('Received an invalid REST network packet');
							continue;
						}
						
						const restPacket: IRestNetworkPacket = decoded as IRestNetworkPacket;
						this.since = Math.max(this.since, restPacket.packet);

						packet = restPacket as ICommonsNetworkPacket;
						if (packet.data !== undefined) packet.data = JSON.parse(packet.data);
					}
					
					this.incoming(packet);
				}
			} finally {
				completed = true;
			}
		}, interval);
	}
			
	public setActive(state: boolean): void {
		this.active = state;
		this.since = -1;
	}
	
	public async transmit(packet: ICommonsNetworkPacket): Promise<any> {
		let encoded: TEncodedObject;
		
		if (this.compress) {
			const compressed: ICommonsNetworkPacketCompressed = compressPacket(packet);
			encoded = CommonsType.encodePropertyObject(compressed);
			if (encoded.z !== undefined) encoded.z = JSON.stringify(encoded.z);
		} else {
			encoded = CommonsType.encodePropertyObject(packet);
			if (encoded.data !== undefined) encoded.data = JSON.stringify(encoded.data);
		}
		
		return await this.restService.post<boolean>(`packets?channel=${this.getChannel()}`, encoded);
	}
}
