import { HttpClient } from '@angular/common/http';
import { EventEmitter } from '@angular/core';

import { ICommonsNetworkHandshake } from 'tscommons-network';

import { CommonsNetworkMultiService } from 'ngx-httpcommons-network';
import { CommonsNetworkMultiSocketIoService } from 'ngx-httpcommons-network';

import { AdamantineNetworkRestService } from './adamantine-network-rest.service';

export class AdamantineNetworkHybridService extends CommonsNetworkMultiService {
	private restService: AdamantineNetworkRestService;
	private socketIoService: CommonsNetworkMultiSocketIoService;
	
	constructor(
			restUrl: string,
			http: HttpClient,
			interval: number,
			socketIoUrls: string[],
			compress: boolean = false
	) {
		super();

		super.addPeer(
			this.restService = new AdamantineNetworkRestService(
					restUrl,
					http,
					interval,
					compress
			)
		);

		super.addPeer(
			this.socketIoService = new CommonsNetworkMultiSocketIoService(
					socketIoUrls,
					compress
			)
		);
	}

	public socketIoConnectObservable(): EventEmitter<void> {
		return this.socketIoService.connectObservable();
	}

	public socketIoDisconnectObservable(): EventEmitter<void> {
		return this.socketIoService.disconnectObservable();
	}

	public socketIoPeerNumberChangedObservable(): EventEmitter<number> {
		return this.socketIoService.peerNumberChangedObservable();
	}
	
	public socketIoTriggerPeerNumberChanged(): void {
		this.socketIoService.triggerPeerNumberChanged();
	}

	public connect(params?: ICommonsNetworkHandshake): boolean {
		this.restService.setActive(true);
		return this.socketIoService.connect(params);
	}
}
