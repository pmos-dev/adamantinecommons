import { HttpClient } from '@angular/common/http';

import { TPropertyObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';

import { CommonsAuthorizedRestService } from 'ngx-httpcommons-rest';
import { IAdamantineField } from 'ngx-adamantinecommons-core';

import { AdamantineRestService } from './adamantine-rest.service';

export abstract class AdamantineAuthorizedRestService extends CommonsAuthorizedRestService {

	// can't do multiple inheritence in AdamantineRestService, so have to do pass-through to static methods here
	public static buildFieldLookup(fields: IAdamantineField[]): Map<string, IAdamantineField> {
		return AdamantineRestService.buildFieldLookup(fields);
	}

	public static parseData(incoming: any, types: IAdamantineField[]): TPropertyObject {
		return AdamantineRestService.parseData(incoming, types);
	}
	
	public static encodeData(data: TPropertyObject, types: IAdamantineField[]): TEncodedObject {
		return AdamantineRestService.encodeData(data, types);
	}

	constructor(
			rootURL: string,
			http: HttpClient
	) {
		super(rootURL, http);
	}

}
