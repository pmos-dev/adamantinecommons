import { HttpClient } from '@angular/common/http';

import { ECommonsAuthorizationMethod } from 'ngx-httpcommons-rest';

import { AdamantineAuthorizedRestService } from './adamantine-authorized-rest.service';

export abstract class AdamantineKeyAuthorizedRestService extends AdamantineAuthorizedRestService {

	constructor(
			rootURL: string,
			http: HttpClient
	) {
		super(rootURL, http);
	}

	public setKey(key: string): void {
		this.setAuthorization(ECommonsAuthorizationMethod.KEY, key);
	}

	public clearKey(): void {
		this.setAuthorization(undefined, undefined);
	}
}
