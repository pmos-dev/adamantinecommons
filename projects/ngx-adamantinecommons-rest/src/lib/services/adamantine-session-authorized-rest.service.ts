import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

import { ECommonsAuthorizationMethod } from 'ngx-httpcommons-rest';

import { AdamantineAuthorizedRestService } from './adamantine-authorized-rest.service';

export abstract class AdamantineSessionAuthorizedRestService extends AdamantineAuthorizedRestService {

	constructor(
			rootURL: string,
			http: HttpClient
	) {
		super(rootURL, http);
	}

	public setSession(uid: string): void {
		this.setAuthorization(ECommonsAuthorizationMethod.SESSION, uid);
	}

	public clearSession(): void {
		this.setAuthorization(undefined, undefined);
	}

	public async validate(): Promise<boolean> {
		try {
			const rest: boolean = await this.get<boolean>(`sessions/validate`);
			CommonsType.assertBoolean(rest);
			
			return rest === true;
		} catch (e) {
			return false;
		}
	}
}
