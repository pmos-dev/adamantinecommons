import { HttpClient } from '@angular/common/http';

import { TEncodedObject } from 'tscommons-core';

import { AdamantineRestService } from './adamantine-rest.service';

export abstract class AdamantineExternalKeyRestService extends AdamantineRestService {

	private externalKey: string;

	constructor(
		rootURL: string,
		http: HttpClient
	) {
		super(rootURL, http);
		
		this.externalKey = '';
	}

	public setExternalKey(key: string): void {
		this.externalKey = key;
	}

	private extendScript(script: string): string {
		const char: string = -1 !== script.indexOf('?') ? '&' : '?';
		return `${script}${char}key=${this.externalKey}`;
	}
	
	protected async internalGet<T>(script: string, params?: TEncodedObject, maxReattempts?: number): Promise<T> {
		return await super.internalGet<T>(
				this.extendScript(script),
				params,
				maxReattempts
		);
	}
	
	protected async internalPost<T>(script: string, body: TEncodedObject, params?: TEncodedObject): Promise<T> {
		return await super.internalPost<T>(
				this.extendScript(script),
				body,
				params
		);
	}
	
	protected async internalPut<T>(script: string, body: TEncodedObject, params?: TEncodedObject): Promise<T> {
		return await super.internalPut<T>(
				this.extendScript(script),
				body,
				params
		);
	}
	
	protected async internalPatch<T>(script: string, body: TEncodedObject, params?: TEncodedObject): Promise<T> {
		return await super.internalPatch<T>(
				this.extendScript(script),
				body,
				params
		);
	}
	
	protected async internalDelete<T>(script: string, params?: TEncodedObject): Promise<T> {
		return await super.internalDelete<T>(
				this.extendScript(script),
				params
		);
	}
}
