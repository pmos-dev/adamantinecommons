import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';
import { CommonsDate } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';

import { CommonsRestService } from 'ngx-httpcommons-rest';
import { IAdamantineField, EAdamantineFieldType } from 'ngx-adamantinecommons-core';

export abstract class AdamantineRestService extends CommonsRestService {
	
	public static buildFieldLookup(fields: IAdamantineField[]): Map<string, IAdamantineField> {
		const map: Map<string, IAdamantineField> = new Map<string, IAdamantineField>();
		
		for (const field of fields) map.set(field.name, field);
		
		return map;
	}

	public static parseData(incoming: any, types: IAdamantineField[]): TPropertyObject {
		if (!CommonsType.isEncodedObject(incoming)) throw new Error('Incoming data is not TEncodedObject');
		const encoded: TPropertyObject = incoming as TEncodedObject;
		
		// don't use decodeObject, as it might try and parse string dates into dates. Do it manually here instead.
		
		const map: Map<string, IAdamantineField> = AdamantineRestService.buildFieldLookup(types);
		
		const data: TPropertyObject = {};
		for (const key of Object.keys(encoded)) {
			if (!map.has(key)) throw new Error('No such type exists for the given key');
			if (encoded[key] === null) continue;
			
			const type: IAdamantineField|undefined = map.get(key);
			if (type === undefined) continue;
			
			switch (type.type) {
				case EAdamantineFieldType.SINGLE:
				case EAdamantineFieldType.MULTI:
				case EAdamantineFieldType.PHONE:
				case EAdamantineFieldType.EMAIL:
				case EAdamantineFieldType.URL:
					data[key] = encoded[key].toString();
					break;
				case EAdamantineFieldType.BOOLEAN:
					data[key] = encoded[key] === true || [
							'true', 'True', 'TRUE',
							'yes', 'Yes', 'YES',
							'on', 'On', 'ON',
							'1'
					].includes(encoded[key].toString());
					break;
				case EAdamantineFieldType.INTEGER:
					data[key] = parseInt(encoded[key].toString(), 10);
					break;
				case EAdamantineFieldType.FLOAT:
					data[key] = parseFloat(encoded[key].toString());
					break;
				case EAdamantineFieldType.DATE:
					data[key] = CommonsDate.dmYToDate(encoded[key].toString());
					break;
				case EAdamantineFieldType.TIME:
					data[key] = CommonsDate.HiToDate(encoded[key].toString());
					break;
				case EAdamantineFieldType.DATETIME:
					data[key] = CommonsDate.dmYHiToDate(encoded[key].toString());
					break;
				case EAdamantineFieldType.LISTSET:
					data[key] = encoded[key].toString();
					break;
				default:
					// allow fall-through so subclasses can handle additional custom types
			}
		}
		
		return data;
	}
	
	public static encodeData(data: TPropertyObject, types: IAdamantineField[]): TEncodedObject {
		const map: Map<string, IAdamantineField> = AdamantineRestService.buildFieldLookup(types);
		
		const outgoing: TPropertyObject = {};
		for (const key of Object.keys(data)) {
			if (!map.has(key)) throw new Error('No such type exists for the given key');
			
			if (data[key] === null) {
				outgoing[key] = null;
				continue;
			}
			
			const type: IAdamantineField|undefined = map.get(key);
			if (type === undefined) continue;
			
			switch (type.type) {
				case EAdamantineFieldType.SINGLE:
				case EAdamantineFieldType.MULTI:
				case EAdamantineFieldType.PHONE:
				case EAdamantineFieldType.EMAIL:
				case EAdamantineFieldType.URL:
					outgoing[key] = data[key].toString();
					break;
				case EAdamantineFieldType.BOOLEAN:
					outgoing[key] = data[key] ? true : false;
					break;
				case EAdamantineFieldType.INTEGER:
					outgoing[key] = parseInt(data[key].toString(), 10);
					break;
				case EAdamantineFieldType.FLOAT:
					outgoing[key] = parseFloat(data[key].toString());
					break;
				case EAdamantineFieldType.DATE:
					if (!CommonsType.isDate(data[key])) throw new Error('Value is not a date');
					outgoing[key] = CommonsDate.dateTodmY(data[key] as Date);
					break;
				case EAdamantineFieldType.TIME:
					if (!CommonsType.isDate(data[key])) throw new Error('Value is not a date');
					outgoing[key] = CommonsDate.dateToHi(data[key] as Date);
					break;
				case EAdamantineFieldType.DATETIME:
					if (!CommonsType.isDate(data[key])) throw new Error('Value is not a date');
					outgoing[key] = CommonsDate.dateTodmYHi(data[key] as Date);
					break;
				case EAdamantineFieldType.LISTSET:
					outgoing[key] = data[key].toString();
					break;
				default:
					// allow fall-through so subclasses can handle additional custom types
			}
		}
		
		return CommonsType.encodePropertyObject(outgoing);
	}

	constructor(
			rootURL: string,
			http: HttpClient
	) {
		super(rootURL, http);
	}

}
