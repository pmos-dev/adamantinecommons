import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';

import { ECommonsAuthorizationMethod } from 'ngx-httpcommons-rest';

import { AdamantineAuthorizedRestService } from './adamantine-authorized-rest.service';

// Prefers sessions over keys, but falls back to keys on the absence of a session

export abstract class AdamantineHybridAuthorizedRestService extends AdamantineAuthorizedRestService {
	private key: string|undefined;
	private session: string|undefined;
	
	constructor(
		rootURL: string,
		http: HttpClient
	) {
		super(rootURL, http);
	}

	public setKey(key: string): void {
		this.key = key;
	}

	public clearKey(): void {
		this.key = undefined;
	}

	public setSession(uid: string): void {
		this.session = uid;
	}

	public clearSession(): void {
		this.session = undefined;
	}
	
	public hasSession(): boolean {
		return this.session !== undefined;
	}
	
	protected async internalGet<T>(
			script: string,
			params?: TEncodedObject,
			maxReattempts?: number,
			headers?: TEncodedObject
	): Promise<T> {
		let method: ECommonsAuthorizationMethod|undefined;
		let value: string|undefined;

		if (this.session !== undefined) {
			method = ECommonsAuthorizationMethod.SESSION;
			value = this.session;
		} else if (this.key !== undefined) {
			method = ECommonsAuthorizationMethod.KEY;
			value = this.key;
		}

		return await this.fixedAuthorizationGet<T>(
				method,
				value,
				script,
				params,
				maxReattempts,
				headers
		);
	}
	
	protected async internalPost<T>(
			script: string,
			body: TEncodedObject,
			params?: TEncodedObject,
			headers?: TEncodedObject
	): Promise<T> {
		let method: ECommonsAuthorizationMethod|undefined;
		let value: string|undefined;

		if (this.session !== undefined) {
			method = ECommonsAuthorizationMethod.SESSION;
			value = this.session;
		} else if (this.key !== undefined) {
			method = ECommonsAuthorizationMethod.KEY;
			value = this.key;
		}

		return await this.fixedAuthorizationPost<T>(
				method,
				value,
				script,
				body,
				params,
				headers
		);
	}
	
	protected async internalPut<T>(
			script: string,
			body: TEncodedObject,
			params?: TEncodedObject,
			headers?: TEncodedObject
	): Promise<T> {
		let method: ECommonsAuthorizationMethod|undefined;
		let value: string|undefined;

		if (this.session !== undefined) {
			method = ECommonsAuthorizationMethod.SESSION;
			value = this.session;
		} else if (this.key !== undefined) {
			method = ECommonsAuthorizationMethod.KEY;
			value = this.key;
		}

		return await this.fixedAuthorizationPut<T>(
				method,
				value,
				script,
				body,
				params,
				headers
		);
	}
	
	protected async internalPatch<T>(
			script: string,
			body: TEncodedObject,
			params?: TEncodedObject,
			headers?: TEncodedObject
	): Promise<T> {
		let method: ECommonsAuthorizationMethod|undefined;
		let value: string|undefined;

		if (this.session !== undefined) {
			method = ECommonsAuthorizationMethod.SESSION;
			value = this.session;
		} else if (this.key !== undefined) {
			method = ECommonsAuthorizationMethod.KEY;
			value = this.key;
		}

		return await this.fixedAuthorizationPatch<T>(
				method,
				value,
				script,
				body,
				params,
				headers
		);
	}
	
	protected async internalDelete<T>(
			script: string,
			params?: TEncodedObject,
			headers?: TEncodedObject
	): Promise<T> {
		let method: ECommonsAuthorizationMethod|undefined;
		let value: string|undefined;

		if (this.session !== undefined) {
			method = ECommonsAuthorizationMethod.SESSION;
			value = this.session;
		} else if (this.key !== undefined) {
			method = ECommonsAuthorizationMethod.KEY;
			value = this.key;
		}

		return await this.fixedAuthorizationDelete<T>(
				method,
				value,
				script,
				params,
				headers
		);
	}

	public async validate(): Promise<boolean> {
		if (!this.hasSession()) return false;
		
		try {
			const rest: boolean = await this.get<boolean>(`sessions/validate`);
			CommonsType.assertBoolean(rest);
			
			return rest === true;
		} catch (e) {
			return false;
		}
	}
}
