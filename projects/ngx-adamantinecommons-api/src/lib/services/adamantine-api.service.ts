import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';

type TCacheEntry = {
		age: Date;
		data: string;
};

export abstract class AdamantineApiService {
	private static buildParams(params?: {}): HttpParams {
		let hp: HttpParams = new HttpParams();
		
		if (params) {
			for (const key of Object.keys(params)) {
				hp = hp.set(key, params[key]);
			}
		}
		
		return hp;
	}
	
	private cache: Map<string, TCacheEntry>;

	constructor(
			private rootURL: string,
			private http: HttpClient,
			private defaultMaxCacheAge?: number
	) {
		this.cache = new Map<string, TCacheEntry>();
	}

	protected internalGetObservable(script: string, params?: {}, maxCacheAge?: number): Observable<any> {
		const cacheIndex: string = JSON.stringify({
				script: script,
				params: params
		});
			
		maxCacheAge = maxCacheAge || this.defaultMaxCacheAge;
		
		if (maxCacheAge && this.cache.has(cacheIndex)) {
			const threshold: Date = new Date();
			threshold.setMilliseconds(-maxCacheAge);
			
			if (this.cache.has(cacheIndex)) {
				const entry: TCacheEntry|undefined = this.cache.get(cacheIndex);
				if (entry) {
					if (entry.age > threshold) {
						const json = JSON.parse(entry.data);
						return of(json);
					}
					
					// expired
					this.cache.delete(cacheIndex);
				}
			}
		}

		return this.http
				.get<any>(`${this.rootURL}${script}`, { params: AdamantineApiService.buildParams(params) })
				.pipe(
						map((result: any): any => {
							if (maxCacheAge) {
								this.cache.set(cacheIndex, {
										age: new Date(),
										data: JSON.stringify(result)
								});
							}
							
							return result;
						}),
						take(1)
				);
	}
	
	public getObservable(script: string, params?: {}, maxCacheAge?: number): Observable<any> {
		return this.internalGetObservable(script, params, maxCacheAge);
	}
	
	protected internalPostObservable(script: string, params: {}): Observable<any> {
		return this.http
				.post<any>(`${this.rootURL}${script}`, params)
				.pipe(take(1));
	}
	
	public postObservable(script: string, params: {}): Observable<any> {
		return this.internalPostObservable(script, params);
	}
	
	protected internalPutObservable(script: string, params: {}): Observable<any> {
		return this.http
				.put<any>(`${this.rootURL}${script}`, params)
				.pipe(take(1));
	}
	
	public putObservable(script: string, params: {}): Observable<any> {
		return this.internalPutObservable(script, params);
	}
	
	protected internalPatchObservable(script: string, params: {}): Observable<any> {
		return this.http
				.patch<any>(`${this.rootURL}${script}`, params)
				.pipe(take(1));
	}
	
	public patchObservable(script: string, params: {}): Observable<any> {
		return this.internalPatchObservable(script, params);
	}
	
	protected internalDeleteObservable(script: string): Observable<any> {
		return this.http
				.delete<any>(`${this.rootURL}${script}`)
				.pipe(take(1));
	}
	
	public deleteObservable(script: string): Observable<any> {
		return this.internalDeleteObservable(script);
	}

}
