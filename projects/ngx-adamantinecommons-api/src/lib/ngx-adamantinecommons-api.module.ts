import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AdamantineApiInterceptor } from './interceptors/adamantine-api.interceptor';

// NB, we can't use a forRoot() here, as the interceptors don't get loaded for sub-modules.

@NgModule({
		imports: [
				CommonModule
		],
		providers: [
				{
					provide: HTTP_INTERCEPTORS,
					useClass: AdamantineApiInterceptor,
					multi: true
				}
		]
})
export class NgxAdamantineCommonsApiModule { }
