import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { timeout, map, catchError } from 'rxjs/operators';

import { CommonsType } from 'tscommons-core';

const API_TIMEOUT = 30 * 1000;	// 30 seconds

@Injectable()
export class AdamantineApiInterceptor implements HttpInterceptor {

	constructor() { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(req)
				.pipe(
						timeout(API_TIMEOUT),
						map((event: HttpEvent<any>): string|{}|any => {
							if (event instanceof HttpResponse && CommonsType.isObject(event.body) && CommonsType.hasProperty(event.body, 'outcome')) {
								// assume API response
								
								if (event.body.outcome === 'error') {
									return throwError(event.body.message);
								}
								
								if (event.body.outcome === 'success') {
									return event.clone({ body: event.body.result });
								}
									
								return throwError('Unknown outcome');
							}
				
							// assume not an API response
							return event;
						}),
						catchError(event => {
							if (event instanceof HttpErrorResponse) {
								try {
									const json = JSON.parse(event.error);
									if (-1 !== Object.keys(json).indexOf('outcome')) {
										// assume API response
										
										if (json.outcome === 'error') {
											return throwError(json.message);
										}
										
										return throwError('Unknown outcome');
									}
									
									// assume not an API response
									return throwError(event);
								} catch (e) {
									return throwError(event);
								}
							}
							return throwError(event);
						})
				);
	}
}
