import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsMobileFormModule } from 'ngx-webappcommons-mobile-form';

import { AdamantineEditorFormComponent } from './components/adamantine-editor-form/adamantine-editor-form.component';
import { AdamantineEditorSyncFormComponent } from './components/adamantine-editor-sync-form/adamantine-editor-sync-form.component';

@NgModule({
		imports: [
				CommonModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsFormModule,
				NgxWebAppCommonsMobileModule,
				NgxWebAppCommonsMobileFormModule
		],
		declarations: [
				AdamantineEditorFormComponent,
				AdamantineEditorSyncFormComponent
		],
		exports: [
				AdamantineEditorFormComponent,
				AdamantineEditorSyncFormComponent
		]
})
export class NgxAdamantineCommonsEditorModule { }
