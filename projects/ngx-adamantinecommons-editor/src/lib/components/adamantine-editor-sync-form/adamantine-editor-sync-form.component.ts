import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { IAdamantineField, EAdamantineFieldType } from 'ngx-adamantinecommons-core';

@Component({
	selector: 'adamantine-editor-sync-form',
	templateUrl: './adamantine-editor-sync-form.component.html',
	styleUrls: ['./adamantine-editor-sync-form.component.less']
})
export class AdamantineEditorSyncFormComponent extends CommonsComponent implements OnInit, OnChanges {
	private static snapshot(src: TPropertyObject, fields: IAdamantineField[]): TPropertyObject {
		const dest: TPropertyObject = {};
		
		for (const field of fields) {
			dest[field.name] = undefined;
			if (CommonsType.isBlank(src[field.name])) continue;
			
			switch (field.type) {
				case EAdamantineFieldType.SINGLE:
				case EAdamantineFieldType.MULTI:
				case EAdamantineFieldType.PHONE:
				case EAdamantineFieldType.EMAIL:
				case EAdamantineFieldType.URL:
				case EAdamantineFieldType.INTEGER:
				case EAdamantineFieldType.FLOAT:
				case EAdamantineFieldType.LISTSET:
				case EAdamantineFieldType.BOOLEAN:
					dest[field.name] = src[field.name];
					break;
				case EAdamantineFieldType.DATE:
				case EAdamantineFieldType.TIME:
				case EAdamantineFieldType.DATETIME:
					dest[field.name] = new Date(src[field.name].getTime());
					break;
			}
		}
		
		return dest;
	}

	@Input() fields: IAdamantineField[] = [];
	@Input() data: TPropertyObject|undefined;
	@Input() disabled: boolean = false;
	
	@Output() onSyncClash: EventEmitter<IAdamantineField[]> = new EventEmitter<IAdamantineField[]>(true);
	@Output() onReset: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onSave: EventEmitter<TPropertyObject> = new EventEmitter<TPropertyObject>(true);
	
	snapshot: TPropertyObject = {};
	working: TPropertyObject = {};
	
	syncClash: boolean = false;

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		if (this.data) {
			this.snapshot = AdamantineEditorSyncFormComponent.snapshot(this.data, this.fields);
			this.working = AdamantineEditorSyncFormComponent.snapshot(this.snapshot, this.fields);
		}
	}
		
	ngOnChanges(changes: SimpleChanges): void {
		if (CommonsType.hasProperty(changes, 'data')) {
			console.log(changes);
			const incoming: TPropertyObject = AdamantineEditorSyncFormComponent.snapshot(changes.data.currentValue, this.fields);

			const clashes: IAdamantineField[] = [];
			
			for (const field of this.fields) {
				const modifiedLocally: boolean = this.isChanged(field);
				const modifiedRemotely: boolean = !CommonsType.isLooselyEqual(incoming[field.name], this.snapshot[field.name]);
				
				if (!modifiedRemotely) continue;
				
				if (!modifiedLocally) {
					// accept update
					this.working[field.name] = incoming[field.name];
				} else {
					// reject
					clashes.push(field);
				}
			}

			if (clashes.length > 0 && !this.syncClash) {
				this.onSyncClash.emit(clashes);
				this.syncClash = true;
			}
			
			this.snapshot = incoming;
		}
	}

	isChanged(field: IAdamantineField): boolean {
		return !CommonsType.isLooselyEqual(this.working[field.name], this.snapshot[field.name]);
	}
	
	isAnyChanged(): boolean {
		for (const field of this.fields) if (this.isChanged(field)) return true;
		
		return false;
	}
	
	getHelpers(): Map<IAdamantineField, string> {
		const helpers: Map<IAdamantineField, string> = new Map<IAdamantineField, string>();
		
		for (const field of this.fields) if (this.isChanged(field)) helpers.set(field, 'Need to save changes');

		return helpers;
	}

	private doReset(): void {
		this.syncClash = false;
		this.working = AdamantineEditorSyncFormComponent.snapshot(this.snapshot, this.fields);
		this.onReset.emit();
	}
	
	private doSave(): void {
		const changes: TPropertyObject = {};
		
		for (const field of this.fields) if (this.isChanged(field)) changes[field.name] = this.working[field.name];
		
		// temporarily set the snapshot to the presumably saved version, so that the new changes aren't presumed to be clashes.
		this.snapshot = AdamantineEditorSyncFormComponent.snapshot(this.working, this.fields);

		this.onSave.emit(changes);
	}

	doAction(action: string): void {
		switch (action) {
			case 'save': {
				this.doSave();
				break;
			}
			case 'refresh': {
				this.doReset();
				break;
			}
		}
	}
}
