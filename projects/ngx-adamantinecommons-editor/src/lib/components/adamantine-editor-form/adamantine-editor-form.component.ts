import { Component, Input, Output, EventEmitter } from '@angular/core';

import { TPropertyObject } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { IAdamantineField } from 'ngx-adamantinecommons-core';

@Component({
		selector: 'adamantine-editor-form',
		templateUrl: './adamantine-editor-form.component.html',
		styleUrls: ['./adamantine-editor-form.component.less']
})
export class AdamantineEditorFormComponent extends CommonsComponent {
	// TODO: add explicit constructor

	@Input() fields: IAdamantineField[] = [];
	@Input() disabled: boolean = false;

	@Input() data: TPropertyObject|undefined;
	@Output() dataChange: EventEmitter<TPropertyObject> = new EventEmitter<TPropertyObject>();
	
	@Input() helpers: Map<IAdamantineField, string> = new Map<IAdamantineField, string>();
	@Input() errors: Map<IAdamantineField, string> = new Map<IAdamantineField, string>();
	
	cascadeChange(): void {
		this.dataChange.emit(this.data);
	}
	
	getHelper(field: IAdamantineField): string|undefined {
		if (this.helpers.has(field)) return this.helpers.get(field);
		return undefined;
	}
	
	getError(field: IAdamantineField): string|undefined {
		if (this.errors.has(field)) return this.errors.get(field);
		return undefined;
	}
}
