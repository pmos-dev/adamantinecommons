import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { CommonsAdamantineManagedFirstClassUserModelSessionRestClientService } from 'tscommons-rest-adamantine';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-models-adamantine';
import { ECommonsAdamantineAccess, computeECommonsAdamantineAccess } from 'tscommons-models-adamantine';

export abstract class AdamantineSessionUserHasAccessGuard<
		M extends ICommonsAdamantineManagedFirstClassUser
> implements CanActivate {
	constructor(
			private restService: CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<M>,
			private requiredAccess: ECommonsAdamantineAccess
	) {}
	
	async canActivate(
			_next: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<boolean> {
		// We assume that the session guard has already been run
		
		const sessionUser: M = await this.restService.getSessionUser();
		
		return computeECommonsAdamantineAccess(
				sessionUser.access,
				this.requiredAccess
		);
	}
}
