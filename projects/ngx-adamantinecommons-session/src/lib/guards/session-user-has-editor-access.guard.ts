import { CommonsAdamantineManagedFirstClassUserModelSessionRestClientService } from 'tscommons-rest-adamantine';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-models-adamantine';
import { ECommonsAdamantineAccess } from 'tscommons-models-adamantine';

import { AdamantineSessionUserHasAccessGuard } from './session-user-has-access.guard';

export abstract class AdamantineSessionUserHasEditorAccessGuard<
		M extends ICommonsAdamantineManagedFirstClassUser
> extends AdamantineSessionUserHasAccessGuard<M> {
	constructor(
			restService: CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<M>
	) {
		super(
				restService,
				ECommonsAdamantineAccess.EDITOR
		);
	}
}
