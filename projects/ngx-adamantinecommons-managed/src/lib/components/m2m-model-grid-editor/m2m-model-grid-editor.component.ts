import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { ICommonsManaged } from 'tscommons-models-adamantine';
import { ICommonsManagedM2MLinkTableModelMetadata } from 'tscommons-models-adamantine';
import { ECommonsAdamantineAccess, computeECommonsAdamantineAccess } from 'tscommons-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ECommonsTable2Selectable } from 'ngx-webappcommons-table';

import { TM2MLinkPair } from '../../types/tm2m-link-pair';

@Component({
		selector: 'adamantine-m2m-model-grid-editor',
		templateUrl: './m2m-model-grid-editor.component.html',
		styleUrls: ['./m2m-model-grid-editor.component.less']
})
export class M2MModelGridEditorComponent extends CommonsComponent implements OnChanges {
	ECommonsTable2Selectable = ECommonsTable2Selectable;
	
	@Input() metadata: ICommonsManagedM2MLinkTableModelMetadata|undefined;
	@Input() sessionAccess?: ECommonsAdamantineAccess;
	@Input() paginate: number|undefined;
	@Input() paginateChange: EventEmitter<number|undefined> = new EventEmitter<number|undefined>(true);

	@Input() as: ICommonsManaged[] = [];
	@Input() bs: ICommonsManaged[] = [];
	
	@Input() selecteds: TM2MLinkPair[] = [];
	
	@Output() private onLink: EventEmitter<TM2MLinkPair> = new EventEmitter<TM2MLinkPair>(true);
	@Output() private onUnlink: EventEmitter<TM2MLinkPair> = new EventEmitter<TM2MLinkPair>(true);

	from: number = 0;
	page: ICommonsManaged[] = [];
	pageSelecteds: boolean[][] = [];
	
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.as || changes.bs || changes.paginate || changes.selecteds) {
			this.repaginate();
		}
	}
	
	hasAccessToManage(): boolean {
		if (!this.sessionAccess) return true;
		
		return computeECommonsAdamantineAccess(
			this.sessionAccess,
			this.metadata.accessRequiredToManage
		);
	}
	
	doPaginate(): void {
		this.paginateChange.emit(this.paginate);
		this.repaginate();
	}
	
	repaginate(): void {
		if (!this.paginate) {
			this.page = this.as;
		} else {
			if ((this.from + this.paginate) >= this.as.length) this.from = this.as.length - this.paginate;
			if (this.from < 0) this.from = 0;
			
			this.page = this.as.slice(
					this.from,
					this.from + this.paginate
			);
		}
		
		this.pageSelecteds = this.page
				.map((a: ICommonsManaged): boolean[] => {
					return this.bs
							.map((b: ICommonsManaged): boolean => {
								const selected: TM2MLinkPair = this.selecteds
										.find((s: TM2MLinkPair): boolean => s.a.id === a.id && s.b.id === b.id);
								return selected !== undefined;
							});
				});
	}
	
	doFirst(): void {
		if (!this.paginate) return;
		
		this.from = 0;
		this.repaginate();
	}
	
	doBack(): void {
		if (!this.paginate) return;
		if (this.from === 0) return;
		
		this.from -= this.paginate;
		this.repaginate();
	}
	
	doForward(): void {
		if (!this.paginate) return;
		if ((this.from + this.paginate) >= this.as.length) return;
		
		this.from += this.paginate;
		this.repaginate();
	}
	
	doLast(): void {
		if (!this.paginate) return;
		
		this.from = this.as.length - this.paginate;
		this.repaginate();
	}
	
	doCheckedChange(a: ICommonsManaged, b: ICommonsManaged, ai: number, bi: number): void {
		if (!this.hasAccessToManage()) return;
		
		const selected: boolean = this.pageSelecteds[ai][bi];
		if (selected) {
			this.onUnlink.emit({
					a: a,
					b: b
			});
		} else {
			this.onLink.emit({
					a: a,
					b: b
			});
		}
	}
}
