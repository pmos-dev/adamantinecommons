import { Component } from '@angular/core';
import { Input } from '@angular/core';

import { CommonsString } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { ICommonsManagedModelDataField } from 'tscommons-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'adamantine-data-fields-form',
		templateUrl: './data-fields-form.component.html',
		styleUrls: ['./data-fields-form.component.less']
})
export class DataFieldsFormComponent extends CommonsComponent {
	@Input() dataFields: ICommonsManagedModelDataField[] = [];
	@Input() data: TPropertyObject = {};
	@Input() disabled: boolean = false;
	@Input() autoDropdownDirection: boolean = true;
	@Input() private ucFieldNames: boolean = false;
	
	getPlaceholder(field: ICommonsManagedModelDataField): string|undefined {
		const optional: string = field.optional ? ' (optional)' : '';
		
		let label: string|undefined = field.description || field.name || '';

		if (this.ucFieldNames) label = CommonsString.ucWords(label);
		
		if (optional) label = `${label} (optional)`;
		
		return label;
	}
}
