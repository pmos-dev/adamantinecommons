import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { CommonsArray } from 'tscommons-core';
import { ICommonsManaged } from 'tscommons-models-adamantine';
import { ICommonsManagedModelMetadata } from 'tscommons-models-adamantine';
import { ECommonsAdamantineManagedDataType } from 'tscommons-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ECommonsTable2Selectable } from 'ngx-webappcommons-table';

@Component({
		selector: 'adamantine-managed-model-list',
		templateUrl: './managed-model-list.component.html',
		styleUrls: ['./managed-model-list.component.less']
})
export class ManagedModelListComponent extends CommonsComponent implements OnChanges {
	ECommonsAdamantineManagedDataType = ECommonsAdamantineManagedDataType;
	
	@Input() metadata: ICommonsManagedModelMetadata|undefined;
	@Input() rows: ICommonsManaged[] = [];
	@Input() disabled: boolean = false;
	@Input() rowActions: boolean = false;
	@Input() allowMove: boolean|undefined;
	@Input() selectable: ECommonsTable2Selectable|undefined;
	@Input() hasAccessToManage: boolean = false;
	@Input() columnWidths: { [field: string]: string } = {};
	@Input() readOnlyRows: ICommonsManaged[] = [];

	@Input() paginate: number|undefined;
	@Input() paginateChange: EventEmitter<number|undefined> = new EventEmitter<number|undefined>(true);
	
	@Input() selecteds: ICommonsManaged[] = [];
	@Output() selectedsChange = new EventEmitter<ICommonsManaged[]>(true);

	@Output() onActionEdit: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() onActionDelete: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() onActionMoveUp: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() onActionMoveDown: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() onActionMoveTop: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() onActionMoveBottom: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);

	from: number = 0;
	page: ICommonsManaged[] = [];

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.rows || changes.paginate) {
			this.repaginate();
		}
	}
	
	getColumnWidth(field: string): string {
		return this.columnWidths[field] || '1fr';
	}
	
	isSelected(row: ICommonsManaged): boolean {
		if (this.selectable === undefined) return false;
		
		const ids: number[] = this.selecteds
				.map((selected: ICommonsManaged): number => selected.id);
				
		return ids.includes(row.id);
	}
	
	isReadOnlyRow(row: ICommonsManaged): boolean {
		if (this.readOnlyRows.length === 0) return false;
		
		return this.readOnlyRows
				.map((r: ICommonsManaged): number => r.id)
				.includes(row.id);
	}
	
	isFirst(row: ICommonsManaged): boolean {
		return this.rows.length > 0 && this.rows[0].id === row.id;
	}
	
	isLast(row: ICommonsManaged): boolean {
		return this.rows.length > 0 && this.rows[this.rows.length - 1].id === row.id;
	}

	doSelected(row: ICommonsManaged, state: boolean): void {
		if (this.selectable === undefined) return;
		
		if (state) {
			const clone: ICommonsManaged[] = [];
			
			if (this.selectable === ECommonsTable2Selectable.MULTIPLE) clone.push(...this.selecteds);
			clone.push(row);
			
			this.selectedsChange.emit(clone);
		} else {
			const clone: ICommonsManaged[] = [ ...this.selecteds ];
			CommonsArray.remove(
					clone,
					row,
					(a: ICommonsManaged, b: ICommonsManaged): boolean => a.id === b.id
			);
			this.selectedsChange.emit(clone);
		}
	}
	
	doSelectAll(): void {
		if (this.selectable !== ECommonsTable2Selectable.MULTIPLE) return;
		
		if (this.selecteds.length === this.rows.length) {
			this.selecteds = [];
		} else {
			this.selecteds = [ ...this.rows ];
		}
		
		this.selectedsChange.emit(this.selecteds);
	}
	
	doPaginate(): void {
		this.paginateChange.emit(this.paginate);
		this.repaginate();
	}
	
	repaginate(): void {
		if (!this.paginate) {
			this.page = this.rows;
		} else {
			if ((this.from + this.paginate) >= this.rows.length) this.from = this.rows.length - this.paginate;
			if (this.from < 0) this.from = 0;
			
			this.page = this.rows.slice(
					this.from,
					this.from + this.paginate
			);
		}
	}
	
	doFirst(): void {
		if (!this.paginate) return;
		
		this.from = 0;
		this.repaginate();
	}
	
	doBack(): void {
		if (!this.paginate) return;
		if (this.from === 0) return;
		
		this.from -= this.paginate;
		this.repaginate();
	}
	
	doForward(): void {
		if (!this.paginate) return;
		if ((this.from + this.paginate) >= this.rows.length) return;
		
		this.from += this.paginate;
		this.repaginate();
	}
	
	doLast(): void {
		if (!this.paginate) return;
		
		this.from = this.rows.length - this.paginate;
		this.repaginate();
	}
}
