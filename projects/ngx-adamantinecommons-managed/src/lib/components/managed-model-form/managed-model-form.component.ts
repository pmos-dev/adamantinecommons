import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { CommonsDate } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { COMMONS_REGEX_PATTERN_EMAIL } from 'tscommons-core';
import { COMMONS_REGEX_PATTERN_URL } from 'tscommons-core';
import { ICommonsManaged } from 'tscommons-models-adamantine';
import { ICommonsManagedModelMetadata } from 'tscommons-models-adamantine';
import { ICommonsManagedModelDataField } from 'tscommons-models-adamantine';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsSnackService } from 'ngx-webappcommons-mobile';

type TValidityOutcome = {
		valid: true;
		toSave: TPropertyObject;
} | {
		valid: false;
		invalidField: ICommonsManagedModelDataField;
};

@Component({
		selector: 'adamantine-managed-model-form',
		templateUrl: './managed-model-form.component.html',
		styleUrls: ['./managed-model-form.component.less']
})
export class ManagedModelFormComponent extends CommonsComponent implements OnInit, OnChanges {
	@Input() metadata: ICommonsManagedModelMetadata|undefined;
	@Input() private row: ICommonsManaged|undefined;
	@Input() autoDropdownDirection: boolean = true;
	@Input() ucFieldNames: boolean = false;

	@Output() private onCreate: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() private onUpdate: EventEmitter<ICommonsManaged> = new EventEmitter<ICommonsManaged>(true);
	@Output() private onCancel: EventEmitter<void> = new EventEmitter<void>(true);

	internalRow: TPropertyObject = {};

	constructor(
			private snackService: CommonsSnackService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.load();
	}
	
	ngOnChanges(_changes: SimpleChanges): void {
		this.load();
	}
	
	private load(): void {
		this.internalRow = !this.row ? {} : { ...this.row };
		
		if (this.metadata) {
			for (const field of this.metadata.manageableFields) {
				if (!CommonsType.hasProperty(this.internalRow, field.name)) {
					// silly typescript hack because !CommonsType.hasProperty resolves to 'test not TPropertyObject'
					(this.internalRow as TPropertyObject)[field.name] = undefined;
				} else {
					switch (field.type) {
						case 'datetime':
							this.internalRow[field.name] = CommonsDate.dateToYmdHis(this.internalRow[field.name]).replace(' ', 'T');
							break;
						case 'date':
							this.internalRow[field.name] = CommonsDate.dateToYmd(this.internalRow[field.name]);
							break;
						case 'time':
							this.internalRow[field.name] = CommonsDate.dateToHi(this.internalRow[field.name]);
							break;
						case 'boolean':
							this.internalRow[field.name] = CommonsType.attemptBoolean(this.internalRow[field.name]) || false;
							break;
					}
				}
			}
		}
	}
	
	private validateForm(): TValidityOutcome {
		const toSave: TPropertyObject = { ...this.internalRow };

		let invalidField: ICommonsManagedModelDataField|undefined;
		
		for (const field of this.metadata.manageableFields) {
			if (field.type === 'boolean') toSave[field.name] = CommonsType.attemptBoolean(toSave[field.name]) || false;
			
			if (
					toSave[field.name] === undefined
					|| (CommonsType.isString(toSave[field.name]) && (toSave[field.name] as string).trim() === '')
			) {
				toSave[field.name] = undefined;
				
				if (!field.optional) {
					invalidField = field;
					break;
				}
			} else {
				try {
					switch (field.type) {
						case 'datetime':
							let temp: string = toSave[field.name].replace('T', ' ');
							if (!/[0-9]{2}:[0-9]{2}:[0-9]{2}$/.test(temp)) temp = `${temp}:00`;
							toSave[field.name] = CommonsDate.YmdHisToDate(temp);
							break;
						case 'date':
							toSave[field.name] = CommonsDate.YmdToDate(toSave[field.name]);
							break;
						case 'time':
							toSave[field.name] = CommonsDate.HiToDate(toSave[field.name]);
							break;
						case 'email':
							if (!COMMONS_REGEX_PATTERN_EMAIL.test(toSave[field.name])) throw new Error('Invalid email address');
							break;
						case 'url':
							if (!COMMONS_REGEX_PATTERN_URL.test(toSave[field.name])) throw new Error('Invalid URL');
							break;
					}
				} catch (e) {
					invalidField = field;
					break;
				}
			}
		}
		
		if (invalidField) {
			return {
					valid: false,
					invalidField: invalidField
			};
		}

		return {
				valid: true,
				toSave: toSave
		};
	}
	
	isFormCompleted(): boolean {
		const validityOutcome: TValidityOutcome = this.validateForm();
		
		return validityOutcome.valid;
	}
	
	doSave(): void {
		const validityOutcome: TValidityOutcome = this.validateForm();
		
		if (validityOutcome.valid === false) {	// can't use !valid as the type requires false explicity
			this.snackService.error(`Invalid data for ${validityOutcome.invalidField.description || validityOutcome.invalidField.name}`);
			return;
		}
		if (validityOutcome.valid !== true) throw new Error('ValidityOutcome.valid is not true or false.');
		
		const clone: ICommonsManaged = {
				...this.row,
				...validityOutcome.toSave
		};

		if (!this.row) {
			this.onCreate.emit(clone);
		} else {
			this.onUpdate.emit(clone);
		}
	}
	
	doCancel(): void {
		this.onCancel.emit();
	}
}
