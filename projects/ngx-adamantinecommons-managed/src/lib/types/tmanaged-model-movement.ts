import { ICommonsFirstClass } from 'tscommons-models';
import { ECommonsMoveDirection } from 'tscommons-models';

export type TManagedModelMovement = {
		selecteds: ICommonsFirstClass[];
		direction: ECommonsMoveDirection;
};
