import { ICommonsManaged } from 'tscommons-models-adamantine';

export type TM2MLinkPair = {
		a: ICommonsManaged;
		b: ICommonsManaged;
};
