import { ICommonsManaged } from 'tscommons-models-adamantine';

export type TManagedModelCustomActionEvent = {
		action: string;
		selecteds: ICommonsManaged[];
};
