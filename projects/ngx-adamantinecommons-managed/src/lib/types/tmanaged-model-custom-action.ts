import { ECommonsTable2Selectable } from 'ngx-webappcommons-table';

export type TManagedModelCustomAction = {
		action: string;
		caption: string;
		selectable: ECommonsTable2Selectable|undefined;
		requireAccessToManage: boolean;
		class?: string;
};
