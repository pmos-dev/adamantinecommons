import { Injectable } from '@angular/core';

import { CommonsString } from 'tscommons-core';
import { CommonsHttpConflictError } from 'tscommons-http';
import { CommonsHttpBadRequestError } from 'tscommons-http';
import { CommonsHttpNotFoundError } from 'tscommons-http';
import { CommonsHttpForbiddenError } from 'tscommons-http';
import { CommonsHttpUnauthorizedError } from 'tscommons-http';
import { CommonsHttpError } from 'tscommons-http';

import { CommonsSnackService } from 'ngx-webappcommons-mobile';

@Injectable({
		providedIn: 'root'
})
export class AdamantineCommonsOutcomeSnackService {
	constructor(
			private snackService: CommonsSnackService
	) {}
	
	public created(asset: string): void {
		this.snackService.success(`New ${asset} created successfully`);
	}
	
	public updated(asset: string): void {
		this.snackService.success(`Changes to ${asset} saved successfully`);
	}
	
	public deleted(asset: string): void {
		this.snackService.success(`${CommonsString.ucWords(asset)} deleted successfully`);
	}
	
	public conflict(asset: string): void {
		const vowel: boolean = /^[aeiou]/i.test(asset);
		
		this.snackService.error(`There is already ${vowel ? 'an' : 'a'} ${asset} with this name or details`);
	}
	
	public notFound(asset: string): void {
		this.snackService.error(`No such ${asset} exists`);
	}
	
	public badRequest(asset: string): void {
		this.snackService.error(`You supplied some invalid data for this ${asset}`);
	}
	
	public forbidden(asset: string): void {
		this.snackService.error(`You do not have access to perform this task on this ${asset}`);
	}
	
	public unauthorized(): void {
		this.snackService.error(`You are not authorized. You may need to log in again.`);
	}
	
	public autoError(asset: string, e: Error): void {
		if (e instanceof CommonsHttpError) {
			console.log(e);
		}
		if (e instanceof CommonsHttpConflictError) return this.conflict(asset);
		if (e instanceof CommonsHttpBadRequestError) return this.badRequest(asset);
		if (e instanceof CommonsHttpNotFoundError) return this.notFound(asset);
		if (e instanceof CommonsHttpForbiddenError) return this.forbidden(asset);
		if (e instanceof CommonsHttpUnauthorizedError) return this.unauthorized();
		
		this.snackService.error(e.message);
	}
}
