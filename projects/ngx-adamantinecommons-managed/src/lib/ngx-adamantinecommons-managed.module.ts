import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsAppModule } from 'ngx-angularcommons-app';
import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsTableModule } from 'ngx-webappcommons-table';

import { DataFieldsFormComponent } from './components/data-fields-form/data-fields-form.component';
import { ManagedModelListComponent } from './components/managed-model-list/managed-model-list.component';
import { ManagedModelFormComponent } from './components/managed-model-form/managed-model-form.component';
import { ManagedModelComponent } from './components/managed-model/managed-model.component';
import { M2MModelEditorComponent } from './components/m2m-model-editor/m2m-model-editor.component';
import { M2MModelGridEditorComponent } from './components/m2m-model-grid-editor/m2m-model-grid-editor.component';

import { AdamantineCommonsOutcomeSnackService } from './services/outcome-snack.service';

@NgModule({
		imports: [
				CommonModule,
				FormsModule,
				NgxAngularCommonsCoreModule,
				NgxAngularCommonsAppModule,
				NgxAngularCommonsPipeModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsAppModule,
				NgxWebAppCommonsFormModule,
				NgxWebAppCommonsMobileModule,
				NgxWebAppCommonsTableModule
		],
		declarations: [
				DataFieldsFormComponent,
				ManagedModelListComponent,
				ManagedModelFormComponent,
				ManagedModelComponent,
				M2MModelEditorComponent,
				M2MModelGridEditorComponent
		],
		exports: [
				ManagedModelListComponent,
				ManagedModelFormComponent,
				ManagedModelComponent,
				M2MModelEditorComponent,
				M2MModelGridEditorComponent
		]
})
export class NgxAdamantineCommonsManagedModule {
	static forRoot(): ModuleWithProviders<NgxAdamantineCommonsManagedModule> {
		return {
				ngModule: NgxAdamantineCommonsManagedModule,
				providers: [
						AdamantineCommonsOutcomeSnackService
				]
		};
	}
}
