export enum EAdamantineFieldType {
	SINGLE = 'single',
	MULTI = 'multi',
	BOOLEAN = 'boolean',
	INTEGER = 'integer',
	FLOAT = 'float',
	DATE = 'date',
	TIME = 'time',
	DATETIME = 'datetime',
	PHONE = 'phone',
	EMAIL = 'email',
	URL = 'url',
	LISTSET = 'listset'
}
