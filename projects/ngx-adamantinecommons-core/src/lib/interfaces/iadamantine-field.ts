import { CommonsType } from 'tscommons-core';

import { EAdamantineFieldType } from '../enums/eadamantine-field-type';

export interface IAdamantineField {
		name: string;
		description: string;
		type: EAdamantineFieldType|string;
		optional?: boolean;
		allow_multiple?: boolean;
		listset?: string[];
}

export function isIAdamantineField(test: any, ignoreListItems: boolean = false): test is IAdamantineField {
	if (!CommonsType.isObject(test)) return false;
	
	if (!CommonsType.hasPropertyString(test, 'name')) return false;
	if (!CommonsType.hasPropertyString(test, 'description')) return false;
	if (!CommonsType.hasPropertyString(test, 'type')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'optional')) return false;
	
	if (test.type === EAdamantineFieldType.LISTSET) {
		if (!ignoreListItems) if (!CommonsType.hasProperty(test, 'listset') || !CommonsType.isStringArray(test.listset)) return false;
		if (!CommonsType.hasPropertyBoolean(test, 'allow_multiple')) return false;
	}
	
	return true;
}
